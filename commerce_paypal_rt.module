<?php

/**
 * @file
 * Adds a payment method for making PayPal reference transactions.
 */

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_paypal_rt_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['paypal_rt'] = array(
    'base' => 'commerce_paypal_rt',
    'buttonsource' => 'CommerceGuys_Cart_RT',
    'title' => t('PayPal reference transaction'),
    'short_title' => t('PayPal RT'),
    'description' => t('PayPal Reference Transaction'),
    'terminal' => TRUE,
    'offsite' => FALSE,
    'offsite_autoredirect' => FALSE,
  );

  return $payment_methods;
}

/**
 * Returns the default settings for the PayPal WPP payment method.
 */
function commerce_paypal_rt_default_settings() {
  $default_currency = commerce_default_currency();

  return array(
    'api_username' => '',
    'api_password' => '',
    'api_signature' => '',
    'server' => 'sandbox',
    'code' => TRUE,
    'currency_code' => in_array($default_currency, array_keys(commerce_paypal_currencies('paypal_wpp'))) ? $default_currency : 'USD',
    'allow_supported_currencies' => FALSE,
    'txn_type' => COMMERCE_CREDIT_REFERENCE_TXN,
    'log' => array('request' => 0, 'response' => 0),
  );
}

/**
 * Payment method callback: settings form.
 */
function commerce_paypal_rt_settings_form($settings = array()) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $form = array();

  // Merge default settings into the stored settings array.
  $settings = (array) $settings + commerce_paypal_rt_default_settings();

  $form['api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API username'),
    '#default_value' => $settings['api_username'],
  );
  $form['api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API password'),
    '#default_value' => $settings['api_password'],
  );
  $form['api_signature'] = array(
    '#type' => 'textfield',
    '#title' => t('Signature'),
    '#default_value' => $settings['api_signature'],
  );
  $form['server'] = array(
    '#type' => 'radios',
    '#title' => t('PayPal server'),
    '#options' => array(
      'sandbox' => ('Sandbox - use for testing, requires a PayPal Sandbox account'),
      'live' => ('Live - use for processing real transactions'),
    ),
    '#default_value' => $settings['server'],
  );
  $form['currency_code'] = array(
    '#type' => 'select',
    '#title' => t('Default currency'),
    '#description' => t('Transactions in other currencies will be converted to this currency, so multi-currency sites must be configured to use appropriate conversion rates.'),
    '#options' => commerce_paypal_currencies('paypal_wpp'),
    '#default_value' => $settings['currency_code'],
  );
  $form['allow_supported_currencies'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow transactions to use any currency in the options list above.'),
    '#description' => t('Transactions in unsupported currencies will still be converted into the default currency.'),
    '#default_value' => $settings['allow_supported_currencies'],
  );
  $form['log'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => $settings['log'],
  );
  $form['txn_type'] = array(
    '#type' => 'radios',
    '#title' => t('Default credit card transaction type'),
    '#description' => t('The default will be used to process transactions during checkout.'),
    '#options' => array(
      COMMERCE_CREDIT_REFERENCE_TXN => t('Reference transaction'),
    ),
    '#default_value' => $settings['txn_type'],
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_paypal_rt_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $remote_ids = array(t('- Select -'));

  $form['ref_txn'] = array(
    '#tree' => TRUE,
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'commerce_payment') . '/theme/commerce_payment.theme.css'),
    ),
  );

  $payments = commerce_payment_transaction_load_multiple(array(), array('order_id' =>  $order->order_id));

  foreach ($payments as $payment_id => $payment) {
    if (!empty($payment->remote_id)) {
      $remote_ids[$payment->transaction_id] = $payment->remote_id;
    }
  }

  $form['ref_txn']['remote_id_txn'] = array(
    '#type' => 'select',
    '#title' => t('Remote ID'),
    '#options' => $remote_ids,
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'remote_id_amount_update',
      'wrapper' => 'edit-payment-terminal',
      'method' => 'replace',
    ),
  );

  return $form;
}

function remote_id_amount_update($form, $form_state) {
  $transaction_id = $form_state['values']['payment_details']['ref_txn']['remote_id_txn'];
  $transaction = commerce_payment_transaction_load($transaction_id);

  $form['payment_terminal']['#attributes']['id'] = 'edit-payment-terminal';
  $form['payment_terminal']['amount']['#value'] = commerce_paypal_price_amount($transaction->amount, $transaction->currency_code);
  $form['payment_terminal']['currency_code']['#value'] = $transaction->currency_code;

  return $form['payment_terminal'];
}

/**
 * @todo Add actual remote ID validation.
 *
 * Payment method callback: checkout form validation.
 */
function commerce_paypal_rt_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  $valid = TRUE;

  return $valid;
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_paypal_rt_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  module_load_include('module', 'commerce_payment');

  // Display an error and prevent the payment attempt if PayPal WPP has not been
  // configured yet.
  if (empty($payment_method['settings'])) {
    drupal_set_message(t('This payment method must be configured by an administrator before it can be used.'), 'error');
    return FALSE;
  }

  // Ensure we can determine a valid IP address as required by PayPal WPP.
  $ip_address = ip_address();

  // Determine the currency code to use to actually process the transaction,
  // which will either be the default currency code or the currency code of the
  // charge if it's supported by PayPal if that option is enabled.
  $currency_code = $payment_method['settings']['currency_code'];

  if (!empty($payment_method['settings']['allow_supported_currencies']) && in_array($charge['currency_code'], array_keys(commerce_paypal_currencies('paypal_wpp')))) {
    $currency_code = $charge['currency_code'];
  }

  // Convert the charge amount to the specified currency.
  $amount = commerce_currency_convert($charge['amount'], $charge['currency_code'], $currency_code);

  // Load the transaction to add transaction-specific data to the request.
  $transaction = commerce_payment_transaction_load($pane_values['ref_txn']['remote_id_txn']);

  // Build a name-value pair array for this transaction.
  $nvp = array(
    'METHOD' => 'DoReferenceTransaction',
    'PAYMENTACTION' => 'Sale',
    'NOTIFYURL' => commerce_paypal_ipn_url($payment_method['instance_id']),
    'REFERENCEID' => $transaction->remote_id,
    'BUTTONSOURCE' => $payment_method['buttonsource'],
    'AMT' => commerce_paypal_price_amount($amount, $currency_code),
    'CURRENCYCODE' => $currency_code,
  );

  // Build a description for the order.
  $description = array();

  // Add additional transaction invormation to the request array.
  $nvp += array(
    // Order Information; we append the timestamp to the order number to allow
    // for multiple transactions against the same order.
    'INVNUM' => substr($order->order_number, 0, 127) . '-' . REQUEST_TIME,
    'CUSTOM' => substr(t('Order @number', array('@number' => $order->order_number)), 0, 256),
    'DESC' => substr(implode(', ', $description), 0, 127),

    // Customer Information
    'IPADDRESS' => substr($ip_address, 0, (!filter_var($ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) ? 127 : 15),
  );

  // Submit the request to PayPal.
  $response = commerce_paypal_api_request($payment_method, $nvp, $order);

  // Prepare a transaction object to log the API response.
  $transaction = commerce_payment_transaction_new('paypal_rt', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $amount;
  $transaction->currency_code = $currency_code;
  $transaction->payload[REQUEST_TIME] = $response;

  // Build a meaningful response message.
  $message = array();
  $action = commerce_paypal_reverse_payment_action($nvp['PAYMENTACTION']);

  // Set the remote ID and transaction status based on the acknowledgment code.
  switch ($response['ACK']) {
    case 'SuccessWithWarning':
    case 'Success':
      $transaction->remote_id = $response['TRANSACTIONID'];

      // Set the transaction status based on the type of transaction this was.
      switch ($payment_method['settings']['txn_type']) {
        case COMMERCE_CREDIT_REFERENCE_TXN:
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          break;
      }
      $message[] = '<b>' . t('@action - Success', array('@action' => $action)) . '</b>';
      break;

    case 'FailureWithWarning':
    case 'Failure':
    default:
      // Create a failed transaction with the error message.
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $message[] = '<b>' . t('@action - Failure', array('@action' => $action)) . '</b>';
      $message[] = t('@severity @code: @message', array('@severity' => $response['L_SEVERITYCODE0'], '@code' => $response['L_ERRORCODE0'], '@message' => $response['L_LONGMESSAGE0']));
  }

  // Store the type of transaction in the remote status.
  $transaction->remote_status = $nvp['PAYMENTACTION'];

  // Set the final message.
  $transaction->message = implode('<br />', $message);

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);
}
